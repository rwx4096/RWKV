#!/bin/sh

base="time zig build-exe src/main.zig src/vulkan.c -Isrc/ -Ilib/ -I$VULKAN_SDK/include/ -L$VULKAN_SDK/lib/ -lvulkan --name RWKV --color on --cache-dir .cache/ -freference-trace=16"
base_r="$base -OReleaseSmall -fllvm -flld -flto -fstrip -fno-stack-check -fno-stack-protector -fno-unwind-tables" # -fno-compiler-rt -fdll-export-fns

case $1 in
    "d"|"debug")
        $base -ODebug -fvalgrind
        if [ $? = 0 ]; then
            rm RWKV.o
            valgrind --leak-check=full --show-leak-kinds=all ./RWKV
        fi
        ;;
    "r"|"release")
        $base_r
        if [ $? = 0 ]; then
            rm RWKV.o
            strip -R.comment RWKV
            ./RWKV
        fi
        ;;
    "p"|"profile")
        $base_r lib/tracy/public/TracyClient.cpp -lc
        if [ $? = 0 ]; then
            rm RWKV.o
            strace -rtTvyyYCw ./RWKV
            Tracy
        fi
        ;;
    *)
        echo "Usage:"
        echo "  ./build [command]"
        echo ""
        echo "Commands:"
        echo "  d, debug      Builds in debug mode and tests with Valgrind."
        echo "  r, release    Builds in release mode and runs."
        echo "  p, profile    Builds in release mode and profiles with Tracy and Strace."
        echo ""
        ;;
esac
