#pragma once

#include <vulkan/vulkan.h>

typedef struct {
    const enum { err, ok } type;
    const char* const      msg;
} Res;
#define Ok(msg)  (const Res) { ok,  msg }
#define Err(msg) (const Res) { err, msg }

typedef struct {
    VkInstance       inst;
    // VkPhysicalDevice physic_dev;
    // VkDevice         logic_dev;
} Ctx;
// TODO: Zig ignores the static keyword for some reason so this works.
static const Ctx ctx;

const Res  initInst(        Ctx* const);
const void deinitInst(const Ctx* const);
const Res  initDev(   const Ctx* const);
const void deinitDev( const Ctx* const);
