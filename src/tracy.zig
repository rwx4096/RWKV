const std = @import("std");
const c = @cImport({
    @cDefine("TRACY-ENABLE", "");
    @cInclude("tracy/public/tracy/TracyC.h");
});

pub inline fn init(comptime src: std.builtin.SourceLocation, comptime name: ?[*:0]const u8) type {
    const srcloc = &c.__tracy_source_location_data {
        .name = name,
        .function = src.fn_name.ptr,
        .file = src.file.ptr,
        .line = src.line,
        .color = 0,
    };
    return opaque {
        const zone = if (@hasDecl(c, "TRACY_HAS_CALLSTACK")) c.___tracy_emit_zone_begin_callstack(srcloc, 16, 1) else c.___tracy_emit_zone_begin(srcloc, 1);
        pub inline fn deinit(self: @This()) void {
            c.___tracy_emit_zone_end(self.zone);
        }
    };
}
