#include "vulkan.h"

#define ASSERT(cond, msg) if(__builtin_expect(!(cond), 0)) return(Err(msg))

const Res initInst(Ctx* const ctx) {
    ASSERT(VK_SUCCESS == vkCreateInstance(&(const VkInstanceCreateInfo) {
        .sType            = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext            = NULL,
        .flags            = VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR,
        .pApplicationInfo = &(const VkApplicationInfo) {
            .sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pNext              = NULL,
            .pApplicationName   = NULL,
            .applicationVersion = VK_MAKE_VERSION(0, 0, 1),
            .pEngineName        = NULL,
            .engineVersion      = VK_MAKE_VERSION(0, 0, 1),
            .apiVersion         = VK_API_VERSION_1_3,
        },
        .enabledLayerCount       = 0,
        .ppEnabledLayerNames     = NULL,
        .enabledExtensionCount   = 1,
        .ppEnabledExtensionNames = &(const char* const) { VK_KHR_PORTABILITY_ENUMERATION_EXTENSION_NAME },
    }, NULL, &ctx->inst), "Failed to initilize instance.");
    return(Ok(""));
}

const void deinitInst(const Ctx* const ctx) { vkDestroyInstance(ctx->inst, NULL); }

const Res initDev(const Ctx* const ctx) {
    uint32_t physic_dev_num, physic_dev_idx;
    ASSERT(VK_SUCCESS == vkEnumeratePhysicalDevices(ctx->inst, &physic_dev_num, NULL) && 0 != physic_dev_num, 
        "Failed to obtain physical devices.");
    VkPhysicalDevice physic_dev_arr[physic_dev_idx = physic_dev_num];
    ASSERT(VK_SUCCESS == vkEnumeratePhysicalDevices(ctx->inst, &physic_dev_num, physic_dev_arr),
        "Failed to obtain physical devices.");
    while(--physic_dev_idx) {
#define physic_dev physic_dev_arr[physic_dev_idx]
        VkPhysicalDeviceProperties physic_dev_props;
        VkPhysicalDeviceFeatures   physic_dev_feats;
        vkGetPhysicalDeviceProperties(physic_dev, &physic_dev_props);
        vkGetPhysicalDeviceFeatures(  physic_dev, &physic_dev_feats);
        // VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU   == physic_dev_props.deviceType
        // VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU    == physic_dev_props.deviceType
        // VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU == physic_dev_props.deviceType
        // VK_API_VERSION_1_3 >= physic_dev_props.apiVersion
        // physic_dev_feats.logicOp
        // physic_dev_feats.shaderFloat64
        // physic_dev_feats.shaderInt64
        // physic_dev_feats.shaderInt16
        // physic_dev_props.limits.maxComputeSharedMemorySize
    }
    uint32_t queue_fam_props_num, queue_fam_props_idx;
    vkGetPhysicalDeviceQueueFamilyProperties(physic_dev, &queue_fam_props_num, NULL);
    VkQueueFamilyProperties queue_fam_props_arr[queue_fam_props_idx = queue_fam_props_num];
    vkGetPhysicalDeviceQueueFamilyProperties(physic_dev, &queue_fam_props_num, queue_fam_props_arr);
    while(--queue_fam_props_idx) {
#define queue_fam_props queue_fam_props_arr[queue_fam_props_idx]
        // queue_fam_props.queueFlags & VK_QUEUE_COMPUTE_BIT
    }
    return(Ok(""));
}

const void deinitDev(const Ctx* const ctx) {}
