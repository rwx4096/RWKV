const std = @import("std");
const vk = @cImport(@cInclude("vulkan.h"));
// const tracy = @import("tracy.zig");

// TODO: Better function name.
inline fn handleRes(res: vk.Res) void {
    if(vk.err == res.type) {
        std.debug.getStderrMutex().lock();
        defer std.debug.getStderrMutex().unlock();
        // TODO: ANSI escape codes may not work sometimes: https://ziglang.org/documentation/master/std/src/std/io/tty.zig.html
        nosuspend std.io.getStdErr().writer().print("\x1B[0;1;41mError\x1B[0;1m {s}\x1B[0m\n", .{ res.msg }) catch {};
        std.process.exit(1);
    }
}

pub fn main() noreturn {
    defer std.process.exit(0);

    // const zone = tracy.init(@src(), null);
    // defer zone.deinit();

    var ctx = vk.ctx;

    handleRes(vk.initInst(&ctx));
    defer vk.deinitInst(&ctx);

    handleRes(vk.initDev(&ctx));
    defer vk.deinitDev(&ctx);
}
